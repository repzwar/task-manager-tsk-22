package ru.pisarev.tm.enumerated;

import ru.pisarev.tm.api.entity.IHasCreated;
import ru.pisarev.tm.api.entity.IHasName;
import ru.pisarev.tm.api.entity.IHasStartDate;
import ru.pisarev.tm.api.entity.IHasStatus;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", Comparator.comparing(IHasName::getName)),
    CREATED("Sort by created", Comparator.comparing(IHasCreated::getCreated)),
    START_DATE("Sort by date start", Comparator.comparing(IHasStartDate::getStartDate)),
    STATUS("Sort by status", Comparator.comparing(IHasStatus::getStatus));

    private final String displayName;

    private final Comparator comparator;

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
