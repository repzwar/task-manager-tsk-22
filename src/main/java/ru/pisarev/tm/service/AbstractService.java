package ru.pisarev.tm.service;

import ru.pisarev.tm.api.IRepository;
import ru.pisarev.tm.api.IService;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void addAll(final Collection<E> collection) {
        Optional<Collection<E>> optional = Optional.ofNullable(collection);
        optional.ifPresent(repository::addAll);
    }

    @Override
    public E add(final E entity) {
        if (entity == null) return null;
        return repository.add(entity);
    }

    @Override
    public E findById(final String id) {
        Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E removeById(final String id) {
        Optional<String> optionalId = Optional.ofNullable(id);
        return repository.removeById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    public E remove(final E entity) {
        if (entity == null) return null;
        return repository.remove(entity);
    }
}
