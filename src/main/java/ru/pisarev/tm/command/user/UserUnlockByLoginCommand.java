package ru.pisarev.tm.command.user;

import ru.pisarev.tm.command.AbstractCommand;
import ru.pisarev.tm.enumerated.Role;
import ru.pisarev.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockByLogin(login);
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
