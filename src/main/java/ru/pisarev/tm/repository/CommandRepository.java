package ru.pisarev.tm.repository;

import ru.pisarev.tm.api.repository.ICommandRepository;
import ru.pisarev.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandNames() {
        return commands.values().stream()
                .filter(o -> o.name() != null && !o.name().isEmpty())
                .map(AbstractCommand::name)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<String> getCommandArg() {
        return commands.values().stream()
                .filter(o -> o.arg() != null && !o.arg().isEmpty())
                .map(AbstractCommand::arg)
                .collect(Collectors.toList());
    }

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
