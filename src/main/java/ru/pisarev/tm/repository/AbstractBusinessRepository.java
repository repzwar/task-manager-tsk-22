package ru.pisarev.tm.repository;

import ru.pisarev.tm.model.AbstractBusinessEntity;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractBusinessRepository<E extends AbstractBusinessEntity> extends AbstractRepository<E> {

    protected final Map<String, E> entities = new LinkedHashMap<>();

    public List<E> findAll(final String userId) {
        return entities.values().stream()
                .filter(o -> userId.equals(o.getUserId()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return entities.values().stream()
                .filter(o -> userId.equals(o.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public void addAll(final String userId, Collection<E> collection) {
        if (collection == null) return;
        collection.forEach(o -> o.setUserId(userId));
        Map<String, E> newEntities = collection.stream()
                .collect(Collectors.toMap(E::getId, Function.identity(), (o1, o2) -> o1, LinkedHashMap::new));
        entities.putAll(newEntities);
    }

    public E add(final String userId, final E entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        entities.put(entity.getId(), entity);
        return entity;
    }

    public E findById(final String userId, final String id) {
        E entity = entities.get(id);
        if (entity == null) return null;
        if (userId.equals(entity.getId())) return entity;
        return null;
    }

    public void clear(final String userId) {
        findAll(userId).forEach(o -> entities.remove(o.getId()));
    }

    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        entities.remove(entity.getId());
        return entity;
    }

    public E remove(final String userId, final E entity) {
        if (!userId.equals(entity.getUserId())) return null;
        entities.remove(entity.getId());
        return entity;
    }

    public int getSize(final String userId) {
        return findAll(userId).size();
    }

}
