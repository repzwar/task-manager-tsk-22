package ru.pisarev.tm.model;

import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractEntity implements Serializable {

    protected String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
