package ru.pisarev.tm.api.entity;

import ru.pisarev.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
