package ru.pisarev.tm.api.entity;

public interface IWBS extends IHasStartDate, IHasName, IHasCreated, IHasStatus {
}